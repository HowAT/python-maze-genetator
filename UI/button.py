import pygame


class Pole:
    def __init__(self,color,x,y,width,height,text):
        self.color=color
        self.x=x
        self.y=y
        self.width=width
        self.height=height
        self.text=text
        self.wejscie=0
        self.wyjscie=0
        self.odwiedzony=0
        self.odleglosc =0


    def przycisk(self,screen,outline=None):
        if outline:
            # ramka
            pygame.draw.rect(screen, outline, (self.x - 1, self.y - 1, self.width + 2, self.height + 2), 0)
            # przycisk
            pygame.draw.rect(screen, self.color, (self.x, self.y, self.width, self.height), 0)
            # część odpowiedzialna za napis
            czcionka = pygame.font.SysFont("monospace", 30)
            tekst = czcionka.render(self.text, 1, (20, 20, 20))
            # blit rysuje jedną rzecz na drugiej
            screen.blit(tekst, (
            self.x + (self.width / 2 - tekst.get_width() / 2), self.y + (self.height / 2 - tekst.get_height() / 2)))

    def odkliknięcie(self,pozycjaMyszy):
        pass



class button(Pole):

    def odkliknięcie(self,pozycjaMyszy):
        if pozycjaMyszy[0]>self.x and pozycjaMyszy[0]<self.x+self.width:
            if pozycjaMyszy[1]>self.y and pozycjaMyszy[1]<self.y+self.height:
                return True
        return False