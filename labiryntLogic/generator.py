import time
from labiryntLogic.ulitLab import *
import random
import queue
import pygame
from labiryntLogic.wyjatki import *

class Generuj:
    def __init__(self,szerokosc,wysokosc,powierzchnia,pozycja):
        self.x=szerokosc
        self.y=wysokosc
        self.screen=powierzchnia
        self.listaPKT=[]
        self.listaRozwiazan=[]
        self.listaKorytarzy=[]
        self.wejscie=[]
        self.wyjscie=[]
        self.listaPosrednia=[]
        self.przodek=[]
        #self.at=0

    def LabiryntClear(self):
        #tu list comprehensive
        for i in range(self.x):
            self.listaPKT.append([])
            self.listaPKT[i] = [Korytarz((32, 178, 170), 350 + 15 * i, 30 + 15 * j, 15, 15, '') for j in range(self.y)] #1

        for i3 in range(self.x):
            for j3 in range(self.y):
                 self.listaPKT[i3][j3].przycisk(self.screen, (255, 255, 255))

    def wybór_orientacji(self, x, y):
        if x < y:
            orientacja = 1  # HORIZONTAL
            return orientacja
        else:
            if y < x:
                orientacja = 0  # VERTICAL
                return orientacja
            else:
                if random.randint(1, 2) == 1:
                    orientacja = 1  # HORIZONTAL
                    return orientacja
                else:
                    orientacja = 0  # VERTICAL
                    return orientacja

    def generacja(self, startx, starty, index, indey):
        szerokosc = index - startx
        wysokosc = indey - starty
        orientation=self.wybór_orientacji(szerokosc,wysokosc)
        if szerokosc < 1 or wysokosc < 1:
            return

        if orientation:
            wall = random.randint(starty,indey)
            while wall % 2:
                wall = random.randint(starty, indey)
            corridor = random.randint(startx, index)
            while not corridor % 2:
                corridor = random.randint(startx, index)

            for i in range(startx,index+1):
                if i != corridor:
                    self.listaPKT[i][wall] = Ściana((0, 0, 0), 350 + 15 * i, 30 + 15 * wall, 15, 15, '')
            self.generacja(startx , starty, index, wall - 1)
            self.generacja(startx, wall + 1, index , indey)

        else:
            wall = random.randint(startx, index)
            while wall % 2:
                wall = random.randint(startx, index)
            corridor = random.randint(starty, indey)
            while not corridor % 2:
                corridor = random.randint(starty, indey)

            for i in range(starty, indey+1):
                if i != corridor:
                    self.listaPKT[wall][i] = Ściana((0, 0, 0), 350 + 15 * wall, 30 + 15 * i, 15, 15, '')
            self.generacja(startx, starty, wall - 1, indey)
            self.generacja(wall + 1, starty, index, indey)



    def LabFull(self):
        try:
            if self.wejscie==[] or self.wyjscie==[]:
                raise BezWyjsciaException()
            else:
                self.generacja(0,0,self.x-1,self.y-1)
                for i3 in range(self.x):
                    for j3 in range(self.y):
                        self.listaPKT[i3][j3].przycisk(self.screen, (255, 255, 255))
        except BezWyjsciaException:
            pass

    def rozwiązanieilosc(self,start1,start2,stop1,stop2):
        # potrzebne zmienne////////////////////////////////////////////////////////////////////////
        R = self.x  # ilosc rows
        C = self.y  # ilosc column
        RQ = CQ = queue.Queue()  # puste kolejki rows and columns
        przodek=[]
        iloscWykonanychKrokow = 0
        ilosckorytarzy = 1
        ilosckorytarzynext = 0
        koniec = False  # zmienna mówiąca czy udało nam się dojść do pkt końcowego
        # directions
        dir1 = [-1, 1, 0, 0]
        dir2 = [0, 0, 1, -1]
        # dzialanie///////////////////////////////////////////////////////////////////////////////
        RQ.put(start1)#self.wejscie[0]
        CQ.put(start2)#self.wejscie[1]
        self.listaPKT[start1][start2].odwiedzony = 1
        self.listaPKT[start1][start2].odelglosc = 1
        while RQ.qsize() > 0 or CQ.qsize() > 0:
            r = RQ.get()
            c = CQ.get()
            if self.listaPKT[r][c] == self.listaPKT[stop1][stop2]:#self.wyjscie[0],self.wyjscie[1]
                koniec = True
                break

            for i in range(4):
                r2 = r + dir1[i]
                c2 = c + dir2[i]
                if r2 < 0 or c2 < 0:  # sprawdzamy czy pozycja nadal jest w siatce labiryntu
                    continue
                if r2 >= R or c2 >= C:  # to samo co wyzej
                    continue
                if self.listaPKT[r2][c2].odwiedzony == 1:  # sprawdzamy czy obiekt był odwiedzony
                    continue
                if isinstance(self.listaPKT[r2][c2], Ściana):  # sprawdzamy czy obiekt jest ścianą
                    continue
                RQ.put(r2)
                CQ.put(c2)
                self.listaPKT[r2][c2].odwiedzony = 1
                self.listaPKT[r2][c2].odleglosc = self.listaPKT[r][c].odleglosc+1
                self.listaPKT[r2][c2].poprzedni = self.listaPKT[r][c]
                ilosckorytarzynext = ilosckorytarzynext + 1

            ilosckorytarzy=ilosckorytarzy-1
            if ilosckorytarzy==0:
                ilosckorytarzy=ilosckorytarzynext
                ilosckorytarzynext=0
                iloscWykonanychKrokow+=1
        if koniec:
            at = self.listaPKT[r][c]
            while at.odleglosc != 0:
                print(at.x," ",at.y)
                self.przodek.append(at)
                at = at.poprzedni
                print("next")
            self.przodek.reverse()
            print("ilosc krokow algorytmu BFS:",iloscWykonanychKrokow)
            return iloscWykonanychKrokow
        return -1



    def generujRozwiazanie(self):
        self.listaPKT[self.wejscie[0]][self.wejscie[1]]=Korytarz((66,255,33), 350+15*self.wejscie[0], 30+15*self.wejscie[1],15,15,'')
        self.listaPKT[self.wejscie[0]][self.wejscie[1]].przycisk(self.screen, (255, 255, 255))
        self.listaPKT[self.wyjscie[0]][self.wyjscie[1]]=Korytarz((255, 0, 33), 350+15*self.wyjscie[0], 30+15*self.wyjscie[1],15,15,'')
        self.listaPKT[self.wyjscie[0]][self.wyjscie[1]].przycisk(self.screen, (255, 255, 255))
        for i3 in range(self.x):
            for j3 in range(self.y):
                if isinstance(self.listaPKT[i3][j3],Korytarz):
                    self.listaKorytarzy.append(self.listaPKT[i3][j3])
        self.listaRozwiazan.append(self.listaPKT[self.wejscie[0]][self.wejscie[1]])
        self.rozwiązanieilosc(self.wejscie[0],self.wejscie[1],self.wyjscie[0],self.wyjscie[1])
        for i in range(len(self.przodek) - 1):
            self.przodek[i].color = (200, 72, 187)
            self.przodek[i].przycisk(self.screen, (255, 255, 255))
            pygame.display.update()
            time.sleep(0.05)

    def generacjaZPunktami(self):
        for i in range(len(self.listaKorytarzy)):
            self.listaKorytarzy[i].color=(32,178,170)
            self.listaKorytarzy[i].przycisk(self.screen, (255, 255, 255))
        self.listaPKT[self.wejscie[0]][self.wejscie[1]].color=(66, 255, 33)
        self.listaPKT[self.wyjscie[0]][self.wyjscie[1]].color = (255, 0, 33)
        self.listaPKT[self.wejscie[0]][self.wejscie[1]].przycisk(self.screen, (255, 255, 255))
        self.listaPKT[self.wyjscie[0]][self.wyjscie[1]].przycisk(self.screen, (255, 255, 255))

        for j in range(len(self.listaPosrednia)-1):
            self.przodek=[]
            for i3 in range(self.x):
                for j3 in range(self.y):
                    self.listaPKT[i3][j3].odwiedzony = 0
                    self.listaPKT[i3][j3].odleglosc = 0
                    self.listaPKT[i3][j3].poprzedni = 0
            print(self.listaPosrednia[j][0], self.listaPosrednia[j][1], self.listaPosrednia[j+1][0],self.listaPosrednia[j+1][1])
            self.rozwiązanieilosc(self.listaPosrednia[j][0], self.listaPosrednia[j][1], self.listaPosrednia[j+1][0],self.listaPosrednia[j+1][1])
            for i in range(len(self.przodek) - 1):
                self.przodek[i].color = (200, 72, 187)
                self.przodek[i].przycisk(self.screen, (255, 255, 255))
                pygame.display.update()
                time.sleep(0.05)
            self.listaPKT[self.wejscie[0]][self.wejscie[1]].color = (66, 255, 33)
            self.listaPKT[self.wyjscie[0]][self.wyjscie[1]].color = (255, 0, 33)
            self.listaPKT[self.wejscie[0]][self.wejscie[1]].przycisk(self.screen, (255, 255, 255))
            self.listaPKT[self.wyjscie[0]][self.wyjscie[1]].przycisk(self.screen, (255, 255, 255))
            for i in range(len(self.listaPosrednia)):
                if i==0 or i==(len(self.listaPosrednia)-1):
                    pass
                else:
                    self.listaPKT[self.listaPosrednia[i][0]][self.listaPosrednia[i][1]].color = (255, 99, 0)
                    self.listaPKT[self.listaPosrednia[i][0]][self.listaPosrednia[i][1]].przycisk(self.screen, (255, 255, 255))






